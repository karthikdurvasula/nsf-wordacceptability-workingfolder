#!/usr/bin/env python
# Copyright 2021 Kyle Gorman <kgorman@gc.cuny.edu>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Generates English stimulus spreadsheet for the Wordlikeness Project.

The stimuli are written, in marked-up form, to a TSV file printed to stdout.
"""


import csv
import itertools
import sys


from typing import Any, Dict, Iterator, List, Tuple


class Error(Exception):
    """Custom exception for this module."""

    pass


# Stuff about sonority.
FIELDNAMES = [
    "stimulus",
    "grammatical",
    "TL",   # /tl/ (etc.) onsets.
    "CN",   # Consonant-nasal onsets.
    "NC",   # Nasal-consonant onsets.
    "NPA",  # Nasal place assimilation.
    "OVA",  # Obstruent voice assimilation.
    "onset1",
    "onset1.type",
    "nucleus1",
    "coda1",
    "onset2",
    "onset2.type",
    "nucleus2",
    "coda2",
    "template",
]


# Inventories.


VOICELESS_STOP = ["p", "t", "k"]
VOICED_STOP = ["b", "d", "g"]
STOP = VOICELESS_STOP + VOICED_STOP
# I exclude "ʃ" because it has pseudo-/s/-like behaviors (e.g., in
# <shm>-reduplication) and because its spelling is not always obvious.
# I exclude "ʒ" because it poses spelling problems and is quite rare
# by any account.
# I exclude "θ" because it poses spelling problems.
# I exclude "ð" because it poses spelling problems.
FRICATIVE = ["f", "v"]
S = ["s"]
ONSET_NASAL = ["m", "n"]
L = ["l"]
R = ["ɹ"]
LIQUID = L + R
W = ["w"]

# No r-controlled vowels.
# I exclude "ɔɪ" because it poses spelling problems.
# I exclude the (true) diphthongs.
TENSE_VOWEL = ["iː", "eɪ", "ɑ", "oʊ", "uː"]
LAX_VOWEL = ["ɪ", "ɛ", "æ", "ʊ"]


# Sonority scales.
# TODO(kbg): This is not yet integrated into anything.


CLEMENTS_SONORITY = {
    "W": 3,
    "LIQUID": 2,
    "NASAL": 1,
    "OBSTRUENT": 0,
}

PARKER_SONORITY = {
    "W": 12,
    "R": 11,
    "L": 10,
    "NASAL": 7,
    "VOICED_FRICATIVE": 6,
    "VOICED_STOP": 4,
    "VOICELESS_FRICATIVE": 3,
    "VOICED_STOP": 1,
}


# Helpers.
def _make_stimulus(row: Dict[str, Any]) -> str:
    return (
        row.get("onset1", "")
        + row.get("nucleus1", "")
        + row.get("coda1", "")
        + row.get("onset2", "")
        + row.get("nucleus2", "")
        + row.get("coda2", "")
    )


def _check_grammatical(row: Dict[str, Any]) -> bool:
    for feature in ["TL", "CN", "NPA"]:
        if row[feature]:
            return False
    return True


# Filters.


def _parochial_onset_filter(onset: str) -> bool:
    """Implements the "parochial" constraint against *tl, etc.

    Returns:
        True if satisfied, False if disobeyed.
    """
    bad_subseqs = ["tl", "dl", "θl", "ðl"]
    for bad_subseq in bad_subseqs:
        if bad_subseq in onset:
            return False
    return True


def _nasal_place_assimilation_filter(row: Dict[str, Any]) -> bool:
    """Implements nasal place assimilation.

    Returns:
        True if (vacuously) satisfied, and False if disobeyed.
    """
    coda = row["coda1"]
    onset = row["onset2"]
    if not coda or not onset:
        return True
    if coda not in ONSET_NASAL:
        return True
    if onset not in STOP + FRICATIVE + S:
        raise Error(f"Unexpected medial onset: {onset}")
    if coda == "m":
        return onset in ["p", "b", "f", "v"]
    if coda == "n":
        return onset in ["t", "d", "s"]
    if coda in "ŋ":
        return onset in ["k", "g"]
    else:
        raise Error(f"Unexpected medial coda: {coda}")


def _obstruent_voice_assimilation_filter(row: Dict[str, Any]) -> bool:
    """Implements obstruent voice assimilation.

    Returns:
        True if (vacuously) satisfied, and False if disobeyed.
    """
    coda = row["coda1"]
    onset = row["onset2"]
    if not coda or not onset:
        return True
    voiceless = ["p", "f", "t", "s", "k"]
    return coda in voiceless != onset in voiceless


# Generation.


def _initial_onsets() -> Iterator[Dict[str, Any]]:
    for c in VOICELESS_STOP + VOICED_STOP + FRICATIVE + ONSET_NASAL + S:
        yield {
            "onset1": c,
            "onset1.type": "simple",
            "TL": False,
            "CN": False,
            "NC": False,
        }
    for c2 in VOICELESS_STOP + ONSET_NASAL + L + W:
        yield {
            "onset1": "s" + c2,
            "onset1.type": "sC",
            "TL": False,
            "CN": False,
            "NC": False,
        }
    for c1, c2 in itertools.product(STOP + FRICATIVE, LIQUID):
        onset = c1 + c2
        yield {
            "onset1": onset,
            "onset1.type": "CR",
            "TL": _parochial_onset_filter(onset),
            "CN": False,
            "NC": False,
        }
    for c1 in STOP:
        yield {
            "onset1": c1 + "w",
            "onset1.type": "Cw",
            "TL": False,
            "CN": False,
            "NC": False,
        }
    for c1, c2 in itertools.product(STOP, ONSET_NASAL):
        yield {
            "onset1": c1 + c2,
            "onset1.type": "CN",
            "TL": False,
            "CN": True,
            "NC": False,
        }
    for c1, c2 in itertools.product(ONSET_NASAL, STOP):
        yield {
            "onset1": c1 + c2,
            "onset1.type": "NC",
            "TL": False,
            "CN": False,
            "NC": True,
        }


def _medial_onsets() -> Iterator[Dict[str, Any]]:
    for c in STOP:
        yield {"onset2": c}


def main() -> None:
    tsv_writer = csv.DictWriter(
        sys.stdout, delimiter="\t", fieldnames=FIELDNAMES
    )
    tsv_writer.writeheader()
    # TODO(kbg): Instrument with sonority information.
    # TODO(kbg): Excessive nesting. What is to be done though?
    for onset1 in _initial_onsets():
        row = onset1
        # CVC.
        for nucleus1 in TENSE_VOWEL:
            row["nucleus1"] = nucleus1
            for coda1 in ["s", "n"]:
                row["coda1"] = coda1
                row["NPA"] = True
                row["stimulus"] = _make_stimulus(row)
                row["grammatical"] = _check_grammatical(row)
                row["template"] = "CVC"
                tsv_writer.writerow(row)
        # CVCCVC.
        for nucleus1 in LAX_VOWEL:
            row["nucleus1"] = nucleus1
            for onset2 in _medial_onsets():
                row.update(onset2)
                if row["onset1"] == row["onset2"]:
                    continue
                for coda1 in ["s", "m"]:
                    row["coda1"] = coda1
                    row["nucleus2"] = "ɘ"
                    for coda2 in ["s", "n"]:
                        row["coda2"] = coda2
                        if row["coda1"] == row["coda2"]:
                            continue
                        # Actually checks syllable contact cluster wellformedness.
                        row["NPA"] = _nasal_place_assimilation_filter(row)
                        row["OVA"] = _obstruent_voice_assimilation_filter(row)
                        row["stimulus"] = _make_stimulus(row)
                        row["grammatical"] = _check_grammatical(row)
                        row["template"] = "CVCCVC"
                        tsv_writer.writerow(row)


if __name__ == "__main__":
    main()
