library(tidyverse)
library(xlsx)
library(jsonlite)
library(stringr)

#***************
#Functions


#Function to create a list with romanisation as opposed to hangul
#Jimin's coding was funky for syllable initial and syllable final ㅇ
#I am going to simplify to word-initial and word-final to see how well it works out
Romaniser = function(response="ㅅ,ㅡ,    ㅌ,  ㅏ,ㅋ,  ㅣ",romanisations=transcriptions){
  #Removing the line with "ㅇ". Will change it after the iteration 
  romanisations = romanisations %>% filter(!(grepl("syllable",Romanisations)))
  
  #Removing spaces from the transcription
  response = str_replace_all(response," ","")
  
  for(counter in 1:nrow(romanisations)){
    # counter=1
    # print(counter)
    response=str_replace_all(response,romanisations$Hangul[counter],romanisations$Romanisations[counter])
  }
  
  #Now looking for word-initial and word-final "ㅇ" and changing it appropriately
  response = str_replace_all(response,"^ㅇ","#")
  response = str_replace_all(response,"ㅇ$","ng")
  
  # response = "s,eu,th,a,ㅇ,i"
  #Now looking for word-medial cases and changing them after vowels to silence, and then changing the rest to "ng"
  #preceding context = (?<=...); following context = (?=....)
  response = str_replace_all(response,"(?<=[a|e|i|o|u|w|y],)ㅇ(?=,[a|e|i|o|u])","#")
  (response = str_replace_all(response,"ㅇ","ng"))
  
  return(response)
}
# str_replace_all("aabc","(?=a)a","#")
# str_replace_all("makey1.name","(?<=a)k(?=e)","#")

#Data munger
DataMunger = function(data=dataOriginal,TestStart=32,TestStimuliCount=60,inputFileName){
  #Removing the first 31 lines, as they are unnecessary for analysis
  
  dataCleaned = data[-c(1:(TestStart-1)),] %>% 
    filter(trial_type != "html-keyboard-response") %>% 
    filter(!is.na(rt)) %>% #There was an NA row at the bottom of the table in Exp 3b for some reason.
    #Brute force hack to code each pair of rows for each stim with a single number. 
    #This needs to be cleaned up. 
    mutate(CodingForStimulus=c(rep(1:TestStimuliCount,each=2),(TestStimuliCount+1)))
  
  #Response data
  dataResponses = dataCleaned %>% filter(CodingForStimulus!=(TestStimuliCount+1)) %>% 
    select(rt:response,CodingForStimulus) %>% 
    mutate(responseType=ifelse(grepl("TestRecording",stimulus),"ResponsePlaySound","ResponseActual")) %>% 
    mutate(stimulus = gsub("TestRecordings/","",stimulus)) %>% 
    #Fill works only if the cell has NA, so converting blank to NA
    mutate(stimulus = ifelse(stimulus=="",NA,gsub(".wav","",stimulus))) %>% 
    #Copies value from previous cell to the NA cell
    fill(stimulus) %>%
    filter(responseType=="ResponseActual") %>% 
    separate(response,into=c("TranscriptionKorean","Rating"),sep="\",\"") %>% 
    mutate(TranscriptionKorean = gsub("\\{\"P0_Q0\":\"","",TranscriptionKorean)) %>% 
    mutate(Rating = gsub("P0_Q1\":","",Rating)) %>% 
    mutate(Rating = gsub("\\}","",Rating)) %>% 
    mutate(Romanisation = Romaniser(TranscriptionKorean))
  
  #Demographic data
  dataDemographic= dataCleaned %>% filter(CodingForStimulus==TestStimuliCount+1) %>% 
    mutate(ResponseCleaned=gsub("\"","",response)) %>% 
    # ResponseCleaned=gsub("{","",ResponseCleaned)) %>% 
    # separate(response,sep=",",into=as.character(1:7))
    #Used for Exp 3a
    # separate(response,sep=",",into=c("Age","Gender","AgeOfL2Onset","LivedinEnglishSpeakingCountry","OtherForeignLanguages","AgeOfOtherL2Onset","DescribeOtherL2Experience")) %>% 
    # mutate(Age=gsub("\\{\"P0_Q0\":","",Age),
    #        Age=as.numeric(gsub("\"","",Age))) %>% 
    # mutate(AgeOfL2Onset=gsub("\"P0_Q2\":","",AgeOfL2Onset),
    #        AgeOfL2Onset=as.numeric(gsub("\"","",AgeOfL2Onset))) %>% 
    # mutate(AgeOfOtherL2Onset=gsub("\"P0_Q5\":","",AgeOfOtherL2Onset),
    #        AgeOfOtherL2Onset=gsub("\"","",AgeOfOtherL2Onset)) %>% 
    # mutate(Gender = gsub("\"P0_Q1\":","",Gender),
    #        Gender=gsub("\"","",Gender)) %>% 
    # mutate(LivedinEnglishSpeakingCountry = gsub("\"P0_Q3\":","",LivedinEnglishSpeakingCountry),
    #        LivedinEnglishSpeakingCountry=gsub("\"","",LivedinEnglishSpeakingCountry)) %>%
    # mutate(OtherForeignLanguages = gsub("\"P0_Q4\":","",OtherForeignLanguages),
    #        OtherForeignLanguages=gsub("\"","",OtherForeignLanguages)) %>%
    # mutate(DescribeOtherL2Experience = gsub("\"P0_Q6\":","",DescribeOtherL2Experience),
    #        DescribeOtherL2Experience=gsub("\"","",DescribeOtherL2Experience),
    #        DescribeOtherL2Experience=gsub("}","",DescribeOtherL2Experience)) %>% 
    #But, this is likely better
    separate(response,sep=",\"",into=c("Age","Gender","AgeOfL2Onset","LivedinEnglishSpeakingCountry","OtherForeignLanguages","AgeOfOtherL2Onset","DescribeOtherL2Experience")) %>% 
    #Cleaning up the demographic info
    mutate(Age=gsub("\\{\"P0_Q0\":","",Age),
           Age=as.numeric(gsub("\"","",Age))) %>% 
    mutate(AgeOfL2Onset=gsub("P0_Q2\":","",AgeOfL2Onset),
           AgeOfL2Onset=as.numeric(gsub("\"","",AgeOfL2Onset))) %>% 
    mutate(AgeOfOtherL2Onset=gsub("P0_Q5\":","",AgeOfOtherL2Onset),
           AgeOfOtherL2Onset=gsub("\"","",AgeOfOtherL2Onset)) %>% 
    mutate(Gender = gsub("P0_Q1\":","",Gender),
           Gender=gsub("\"","",Gender)) %>% 
    mutate(LivedinEnglishSpeakingCountry = gsub("P0_Q3\":","",LivedinEnglishSpeakingCountry),
           LivedinEnglishSpeakingCountry=gsub("\"","",LivedinEnglishSpeakingCountry)) %>%
    mutate(OtherForeignLanguages = gsub("P0_Q4\":","",OtherForeignLanguages),
           OtherForeignLanguages=gsub("\"","",OtherForeignLanguages)) %>%
    mutate(DescribeOtherL2Experience = gsub("P0_Q6\":","",DescribeOtherL2Experience),
           DescribeOtherL2Experience=gsub("\"","",DescribeOtherL2Experience),
           DescribeOtherL2Experience=gsub("}","",DescribeOtherL2Experience)) %>% 
    select(Age:DescribeOtherL2Experience)
  
  #Filename/Subject
  dataFileName = data.frame(Subject=rep(gsub(".csv","",fileName),nrow(dataDemographic)))
  
  #returning modified dataframe
  cbind(dataFileName,dataResponses,dataDemographic)
}

# dataOriginal=read.csv("Results/Korean-3b/Originals/Converted to reasonable CSVs_K3b_L1/K3b_L1_001.csv")
# cleanedData=DataMunger(data=dataOriginal)

#***************
#Reading in files

#Directory and files
inputDirectoryData = "Results/Korean-3b/Originals/Converted to reasonable CSVs_k3b_"
List = c("L1","L2")
outputDirectory = "Results/Korean-3b/"
outputFileName = "Korean-3b_"

#Transcription file - hangul and romanisation
transcriptions=read.xlsx("Results/KoreanRomanization.xlsx",sheetIndex=1) %>% 
  rename(Romanisations = Romanization..Yale.Consonants..Revised.Romanization.Vowels.) %>% 
  mutate(Hangul = as.character(Hangul),
         Romanisations = as.character(Romanisations))


#iterating through each list folder
for(list in List){
  filenamesList=list.files(path=paste0(inputDirectoryData,list),pattern="*.csv")  

    
  #iterating through each file in the list
  fullDataForList = NULL
  # list="L1"
  for(fileName in filenamesList){
    #Actual data
    dataOriginal=read.csv(paste0(inputDirectoryData,list,"/",fileName))
    
    fullDataForList = rbind(fullDataForList,DataMunger(data=dataOriginal,inputFileName=fileName))
    # print(fileName)
  }

  #Final data.frame
  # inputFilename = str_replace_all(inputFilename,".csv","")
  # write.csv(fullDataForList,paste0(outputDirectory,outputFileName,list,".csv"),row.names=F,fileEncoding = "UTF-8")
  write_excel_csv(fullDataForList,paste0(outputDirectory,outputFileName,list,".csv"))
}

#***************


