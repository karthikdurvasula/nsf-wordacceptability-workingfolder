library(tidyverse)
library(jsonlite)

inputDir = "/Users/Karthik/Documents/Research/Deconstructing Wordlikeliness Judgements/Experiment Files/Korean - 3a/Stim lists from Kyle/"
testRecordingDir = "TestRecordings/"
inputFile = "kor-list-2.tsv"
outputDir = "/Users/Karthik/Documents/Research/Deconstructing Wordlikeliness Judgements/Experiment Files/Korean - 3a/"
outputFile = "TestStimuli"

stimuli=read_tsv(paste0(inputDir,inputFile)) %>% 
  mutate(WordRomanisation = gsub("t̚","t",transcription),
         WordRomanisation = gsub("p̚","p",WordRomanisation),
         WordRomanisation = gsub("k̚","k",WordRomanisation),
         WordRomanisation = gsub("tʰ","th",WordRomanisation),
         WordRomanisation = gsub("pʰ","ph",WordRomanisation),
         WordRomanisation = gsub("kʰ","kh",WordRomanisation),
         WordRomanisation = gsub("cʰ","ch",WordRomanisation),
         WordRomanisation = gsub("ŋ","ng",WordRomanisation)) %>% 
  mutate(File = paste0(testRecordingDir,WordRomanisation,".wav"))

write(toJSON(stimuli,pretty=T),paste0(outputDir,outputFile,"_",inputFile,".json"))

