Notes: 

Used the samson meteor mic for all the recordings.


****************
#After first recording
1) An unnumbered sound file is the one I think is better, and those that end in "2.wav" are those that I think are a bit worse. In cases, where there is no "2.wav” alternative, that is either because I was confident that the file was good enough, or in one-two cases there were no other good tokens. I noted down the cases that I want to re-record already and didn’t include them in the folder.
2) Also, do we want to keep words such as tlæn.gɪŋ? So, if they wrote “tlanging", we wouldn’t know whether they perceived the n or the ŋ before the g.

****************
# Re-record or replace (2nd attempt)

- dræntʊŋ
- næŋdʊp

List1
- ngiːt				-> mkaɪt
- dɹɪŋgɛp		-> tɹɪndɛs
- tlængɪŋ		-> tlʊmgæk
- sɛŋgʊp 		-> pʊnsɛŋ
- dʊŋgɛm		-> tɛmpʊk
- tɛŋgʊm		-> sʊntɛm

List2
- ngæk			-> ndaɪk
- nkæp			-> mdæk
- sʊngɪm		-> dʊnbɛm
- kɛŋgæm		-> tɛmbɪp
- tɹʊŋgɛm		-> dɹɛmbɪp
- tɛŋgæk		-> sændʊp
- tɹæŋgʊs		-> tɹɪndʊs


****************
# Stims for Re-recording - 3 Sept 2024
- mkaɪt
- tɹɪndɛs
- tlʊmgæk
- pʊnsɛŋ
- tɛmpʊk
- sʊntɛm
- ndaɪk
- dʊnbɛm
- tɛmbɪp
- dɹɛmbɪp
- sændʊp
- tɹɪndʊs
- dræntʊŋ
- næŋdʊp
- gætkʊn
- dlʊpgɪk
- dloʊn


*****************
# Stims for re-recording (3rd attempt)
- daɪp (List 2)
- mdæk (List 2)
- tɹɛŋ (List 2)


*****************
# Stims for re-recording (4th attempt) - Sept 20.
- bɛptʊs












